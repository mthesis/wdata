import delphes_to_image
from argparse import ArgumentParser
import tqdm, ROOT
import numpy as np
import matplotlib.pyplot as plt
import matplotlib, os, h5py
#matplotlib.use('TkAgg')


ROOT.gSystem.Load("libDelphes.so")


def processInput():
    parser = ArgumentParser()
    parser.add_argument("-d", "--dir",
        help="Give Events/ directory in which to look for delphes output")
    parser.add_argument("-f", "--file",
        help="Give root file to evaluate")
    parser.add_argument("--cuts", default=[550., 650., 2.], nargs = 3, type=float,
        help="Cuts set. Enter like: pt_min pt_max eta. Default: 550. 650. 2.")
    parser.add_argument("--deta", default=3.2, type=float,
        help="Delta eta used in pixelation. Default: 3.2")
    parser.add_argument("--dphi", default=3.2, type=float,
        help="Delta phi used in pixelation. Default: 3.2")
    parser.add_argument("--savetag", default="test",
        help="Give a tag to uniquely save the output")
    parser.add_argument("--results", default="./", type=str,
        help="Give path to directory in which to store results, default: ./")
    parser.add_argument("-M", "--momenta", action='store_true',
        help="Get 4-momenta of jet constituents, and do not create images")
    args = parser.parse_args()
    print('\nArguments used:\n{}\n\n'.format(args))
    return args


def getConstituentsMomenta(files, vectors=False, filename=None, cuts=[0, 0, 0]):
    """Function selecting a list of jets and their constituents according to
    given cuts.
    input:
        files - [str], list of filepaths
        filename - str, path to store file, if None, file not stored
        cuts - (3,) float, giving cuts like [pt_min, pt_max, eta_max]
    returns:
        jetsAll - (njets, 200, 3), list of (pt, eta, phi) for each constituent
                                    of all jets
        jetMasses - (njets,), list of jet masses 
    """
    def jetSelection(matching):
        """Function uses branchFatJet and branchParticle and returns list of 
        indices of jets surviving the cuts
        input:
        returns:
            indices - list of indices, indicating selected jets
        """
        if matching:
            dm_partons = []
            for i in range(branchParticle.GetEntriesFast()):
                particle = branchParticle.At(i)
                # if abs(particle.PID) == 4900101 or abs(particle.PID) == 4900102:
                if abs(particle.PID) == 6:
                    dm_partons.append([particle.Eta, particle.Phi, particle.PID])
                    if len(dm_partons) == 2: break
            dm_partons = np.array(dm_partons)

        survivors = []
        for i in range(branchFatJet.GetEntriesFast()):
            jet = branchFatJet.At(i)
            jet = np.array([jet.Eta, jet.Phi, jet.PT])
            # Apply pT and eta cuts
            if jet[2] < cuts[0] or jet[2] > cuts[1] or abs(jet[0]) > cuts[2]:
                continue

            if matching:
                dR = np.sqrt((dm_partons[:, 0] - jet[0]) ** 2 +\
                            (dm_partons[:, 1] - jet[1]) ** 2)
                if dR.min() < 0.8:
                    survivors.append(i)
            else:
                survivors.append(i)

        return survivors


    jetsAll = []
    jetMasses = []
    particlesAll=[]
    nJets = 0

    print("Starting getConstituentsMomenta")
    # Loop over every input file given as an argument
    for file in files:
        # Create chain of root trees
        #chain = ROOT.TChain("Delphes")
        #chain.Add(file)
        fil=ROOT.TFile(file)



        # Create object of class ExRootTreeReader
        treeReader = ROOT.ExRootTreeReader(fil.Delphes)
        numberOfEntries = treeReader.GetEntries()

        print("Chain contains %i Entries" % numberOfEntries)

        # Branches needed for reconstruction of the constituents
        branchEFlowTrack = treeReader.UseBranch("EFlowTrack")
        branchEFlowPhoton = treeReader.UseBranch("EFlowPhoton")
        branchEFlowNeutralHadron = treeReader.UseBranch("EFlowNeutralHadron")
        branchParticle = treeReader.UseBranch("Particle")
        branchFatJet = treeReader.UseBranch("FatJet")
        #branchVertex = treeReader.UseBranch("Vertex")
        #branchGenVertex = treeReader.UseBranch("GenVertex")
        branchTrack = treeReader.UseBranch("Track")


        # Loop over all events        
        for entry in tqdm.tqdm(range(0, numberOfEntries)):
            treeReader.ReadEntry(entry)
            survivors = jetSelection(matching=False)#######matching matters...true for ldm


            #print(type(branchTrack))
            #print(dir(branchTrack))


            #print("found",branchTrack.GetEntriesFast(),"Tracks")
            #for i in range(branchTrack.GetEntriesFast()):
            #    track=branchTrack.At(i)
            #    print(dir(track))
            #    print(type(track))
            #    print(track.L)
            #    exit()

            particles=[]


            for i in range(branchParticle.GetEntriesFast()):
                p=branchParticle.At(i)
                particle=[]
                #setup:
                    #PID,Status,D1,D2,D0,DZ,M1,M2,T,X,Y,Z,E,Px,Py,Pz,PT,Eta,Phi,Mass,Charge
                #print(dir(p))
                #print(type(p))
                #print(p.Status)
                particle.append(p.PID)
                particle.append(p.Status)
                particle.append(p.D1)
                particle.append(p.D2)
                particle.append(p.D0)
                particle.append(p.DZ)
                particle.append(p.M1)
                particle.append(p.M2)
                particle.append(p.T)
                particle.append(p.X)
                particle.append(p.Y)
                particle.append(p.Z)
                particle.append(p.E)
                particle.append(p.Px)
                particle.append(p.Py)
                particle.append(p.Pz)
                particle.append(p.PT)
                particle.append(p.Eta)
                particle.append(p.Phi)
                particle.append(p.Mass)
                particle.append(p.Charge)

                particles.append(particle)



                



            nJets += branchFatJet.GetEntriesFast()
            # Loop over all jets in event
            for i in survivors:
                jet = branchFatJet.At(i)

                # Create array for constituents 
                constituents = []
                pTs = []
                # Loop over jet constituents
                for j in range(0, jet.Constituents.GetEntriesFast()):
                    object = jet.Constituents.At(j)
                    if vectors:
                        lorentzVec = object.P4()
                        constituents.append([
                            lorentzVec.E(), lorentzVec.Px(), lorentzVec.Py(), lorentzVec.Pz()
                        ])
                        pTs.append(lorentzVec.Pt())
                    else:
                        try:
                            constituents.append(
                                [object.PT, object.Eta, object.Phi])
                        except:
                            constituents.append(
                                [object.ET, object.Eta, object.Phi])
                
                # Sort constituents according to their transverse momentum
                if not vectors:
                    constituents.sort(reverse=True)
                    constituents = np.array(constituents)
                    constituents[:, 2] = np.unwrap(constituents[:, 2] - jet.Phi)
                    constituents[:, 1] -= jet.Eta
                else:
                    constituents = np.array(constituents)
                    pTs = np.array(pTs)
                    constituents = constituents[np.argsort(-pTs)]

                temp = np.zeros((200, np.shape(constituents)[1]))
                
                # Fill 200 leading constituents into a "jet vector"
                if np.shape(constituents)[0] < 200:
                    temp[:np.shape(constituents)[0]] = constituents
                else:
                    temp[:] = constituents[:200]
                
                jetsAll.append(temp)
                jetMasses.append(jet.Mass)
                particlesAll.append(particles)
                if len(jetsAll)>=500:
                    basef=f'{args.results}jet_4ext_{args.savetag}/'
                    print("flushing into",basef)
                    if not os.path.isdir(basef):
                        os.mkdir(basef)
                    np.savez_compressed(basef+str(len(os.listdir(basef))),jetsAll=jetsAll,jetMasses=jetMasses,particles=particlesAll)
                    jetsAll=[]
                    jetMasses=[]
                    particlesAll=[]


    print(nJets, 'nJets')
    jetsAll = np.array(jetsAll)
    jetMasses = np.array(jetMasses)
    print(jetsAll.shape)
    print(jetMasses.shape)
    basef=f'{args.results}jet_4ext_{args.savetag}/'
    print("flushing into",basef)
    if not os.path.isdir(basef):
        os.mkdir(basef)
    np.savez_compressed(basef+str(len(os.listdir(basef))),jetsAll=jetsAll,jetMasses=jetMasses,particles=particlesAll)


    exit()
    
    return jetsAll, jetMasses


def advancedPreProcess(jets, filename=None):
    """Given a list of jets, prerocessing steps on constituent level are 
    performed: 1. Shift pT weighted centroid to the origin; 2. Rotate pT 
    weighted principle axis to be vertical; 3. Flip the hardest quadrant to the
    first quadrant.
    input:
        jets: (nJets, 200, 3) - list of (pT, eta, phi) for leading 200 
                                constituents of each jet
        filename: str - Path to store output, if None, output not stored
    returns:
        jets: (nJets, 200, 3) - list of (pT, eta, phi) for leading 200 
                                constituents of each jet after preprocessing
    """
    def min_phi(phi1, phi2):
        return np.arctan2(np.sin(phi1-phi2), np.cos(phi1-phi2))


    def center():
        mean_eta = np.average(constituents[:, 1], weights=constituents[:, 0])
        mean_phi = np.average(constituents[:, 2], weights=constituents[:, 0])

        constituents[:, 2] -= mean_phi
        constituents[:, 1] -= mean_eta        
        
        constituents[:, 2] = (constituents[:, 2] + np.pi) % (2 * np.pi) - np.pi


    def rotate():
        # Calculate the major axis
        eta_coords = constituents[:, 1] * constituents[:, 0]
        phi_coords = constituents[:, 2] * constituents[:, 0]
        coords = np.vstack([eta_coords, phi_coords])
        cov = np.cov(coords)
        evals, evecs = np.linalg.eig(cov)
        sorted_indices = np.argsort(evals)[::-1]
        major_axis = evecs[:, sorted_indices[0]]

        # Rotate major axis to have 0 phi 
        theta = np.arctan(major_axis[0] / major_axis[1])
        c, s = np.cos(theta), np.sin(theta)
        rotation = np.array([[c, s], [-s, c]])
        constituents[:, 1:3] = np.matmul(constituents[:,1:3], rotation)


    def flip():
        quad1 = 0
        quad2 = 0
        quad3 = 0
        quad4 = 0

        for i in range(len(constituents)):
            if constituents[i, 1] > 0:
                if constituents[i, 2] > 0:
                    quad1 += constituents[i, 0]
                else:
                    quad2 += constituents[i, 0]
            else:
                if constituents[i, 2] > 0:
                    quad3 += constituents[i, 0]
                else:
                    quad4 += constituents[i, 0]

        quad = np.argmax([quad1, quad2, quad3, quad4])

        if quad == 1:
            constituents[:, 2] *= -1
        elif quad == 2:
            constituents[:, 1] *= -1
        elif quad == 3:
            constituents[:, 1] *= -1
            constituents[:, 2] *= -1


    print('Started advancedPreProcess')
    # Loop over all jets
    for i in tqdm.tqdm(range(np.shape(jets)[0])):
        constituents = jets[i]

        center()
        rotate()
        flip()

        # Normalise pT of the jet to 1
        constituents[:, 0] /= np.sum(constituents[:, 0])
        jets[i] = constituents

    if filename:
        np.save('/home/thorben/Masterarbeit/Coding/results/' + filename, jets)
    print('Exiting advancedPreProcess, shape: {}'.format(np.shape(jets)))
   
    return jets


def pixelate(jets, jetMasses, dPhi, dEta, filename=None):
    print('Started pixelate')
    # Loop over all jets
    empty_images = []
    indices = []
    empty = 0

    # If file exists, delete it 
    exists = os.path.isfile(filename)
    if exists:
        os.remove(filename)
        exists=False

    with h5py.File(filename, "w") as f:
        images = f.create_dataset('images', 
                shape=(1, 40, 40), 
                chunks=(1, 40, 40),
                maxshape=(None, 40, 40))
        masses = f.create_dataset('masses', 
                shape=(1,), 
                chunks=(1,),
                maxshape=(None,))

        end_i = len(jets)
        index = 0
        for i in tqdm.tqdm(range(end_i)):
            constituents = jets[i]
            mass = jetMasses[i]
            image, _, _ = np.histogram2d(constituents[:, 1], 
                                constituents[:, 2],
                                bins=40,
                                weights=constituents[:,0],  # pt weighted
                                range=[[-dEta / 2., dEta / 2.],
                                        [-dPhi / 2., dPhi / 2.]])

            sum_pT = np.sum(image)
            if sum_pT > 0.95:
                images[index] = image
                masses[index] = mass
                images.resize(images.shape[0] + 1, axis=0)
                masses.resize(masses.shape[0] + 1, axis=0)
                index += 1

            else:
                empty += 1

        images.resize(images.shape[0] - 1, axis=0)
        masses.resize(masses.shape[0] - 1, axis=0)
        print(images.shape)

    print('Images with less than 95 % of pT: ', empty)
    return


if __name__ == "__main__":
    args = processInput()
    delphes_to_image.initialiseROOT()
    files = delphes_to_image.generateListOfFiles(args.file)

    jets, masses = getConstituentsMomenta(files,
        # filename=args.results + 'jets_' + args.savetag,
        vectors=args.momenta,
        cuts=args.cuts)
    print(jets.shape, masses.shape)
    jets = advancedPreProcess(jets, filename=None)
    pixelate(jets, masses, dEta=args.deta, dPhi=args.dphi,
        filename='{}jet_images_{}.h5'.format(args.results, args.savetag))
